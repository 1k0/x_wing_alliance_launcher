#include <stdio.h>
#include <string.h>

#include "cfg_tools.h"

Button CfgButtonTable[XWA_MAX_BUTTONS] =
{
    { BUTTON_D_UP, "Up" },
    { BUTTON_D_RIGHT, "Right" },
    { BUTTON_D_DOWN, "Down" },
    { BUTTON_D_LEFT, "Left" },

    { BUTTON_A, "A" },
    { BUTTON_B, "B" },
    { BUTTON_X, "X" },
    { BUTTON_Y, "Y" },

    { BUTTON_START, "Start" },
    { BUTTON_BACK, "Back" },

    { BUTTON_L1, "L1" },
    { BUTTON_L2, "L2" },
    { BUTTON_L3, "L3" },

    { BUTTON_R1, "R1" },
    { BUTTON_R2, "R2" },
    { BUTTON_R3, "R3" },

    { BUTTON_RA_UP, "Right Analog Up" },
    { BUTTON_RA_RIGHT, "Right Analog Right" },
    { BUTTON_RA_DOWN, "Right Analog Down" },
    { BUTTON_RA_LEFT, "Right Analog Left" },
};

internal Button* GetButtonById(u32 id)
{
    Button* button = NULL;
    for (u32 i = 0; i < XWA_MAX_BUTTONS; ++i)
    {
        button = &CfgButtonTable[i];
        if (button->id == id)
        {
            break;
        }
    }

    return button;
}

Command CfgCommandTable[XWA_MAX_COMMANDS] =
{
    { 0, Command::CATEGORY_NONE, "None" },     //!KEY_NONE!0 None No function
    { 156, Command::CATEGORY_WEAPON, "Fire weapon" },   //!KEY_ALT_2!156 Alt - 2 Fire weapon
    { 99, Command::CATEGORY_WEAPON, "Fire countermeasure" },    //!KEY_C!99 C Fire countermeasure
    { 122, Command::CATEGORY_WEAPON, "Toggle laser convergence" },   //!KEY_Z!122 Z Toggle laser convergence
    { 120, Command::CATEGORY_WEAPON, "Cycle firing settings" },   //!KEY_X!120 X Cycle firing settings
    { 98, Command::CATEGORY_WEAPON, "Beam weapon on / off" },    //!KEY_B!98 B Beam weapon on/off
    { 102, Command::CATEGORY_WEAPON, "Toggle turret automatic firing" },   //!KEY_F!102 F Toggle turret automatic firing
    { 103, Command::CATEGORY_WEAPON, "Toggle pilot / turret position" },   //!KEY_G!103 G Toggle pilot/turret position
    { 119, Command::CATEGORY_ENERGY, "Cycle weapon settings" },   //!KEY_W!119 W Cycle weapon settings
    { 202, Command::CATEGORY_ENERGY, "Adjust beam recharge rate" },   //!KEY_F8!202 F8 Adjust beam recharge rate
    { 203, Command::CATEGORY_ENERGY, "Adjust laser recharge rate" },   //!KEY_F9!203 F9 Adjust laser recharge rate
    { 204, Command::CATEGORY_ENERGY, "Adjust shield recharge rate" },   //!KEY_F10!204 F10 Adjust shield recharge rate
    { 59, Command::CATEGORY_ENERGY, "Transfer shield energy to lasers" },   //!KEY_SEMICOLON!59; (Semicolon)Transfer shield energy to lasers
    { 39, Command::CATEGORY_ENERGY, "Transfer laser energy to shields" },   //!KEY_APOSTROPHE!39 '(Apostrophe) Transfer laser energy to shields
    { 34, Command::CATEGORY_ENERGY, "Transfer all laser energy to shields" },   //!KEY_QUOTES!34 "(Quotes) Transfer all laser energy to shields
    { 215, Command::CATEGORY_ENERGY, "Transfer shield energy to lasers" },   //!KEY_SHIFT_F9!215 SHIFT - F9 Transfer shield energy to lasers
    { 216, Command::CATEGORY_ENERGY, "Transfer laser energy to shields" },   //!KEY_SHIFT_F10!216 SHIFT - F10 Transfer laser energy to shields
    { 217, Command::CATEGORY_ENERGY, "Save power redirect preset #1", },   //!KEY_SHIFT_F11!217 SHIFT_F11 Save power redirect preset #1
    { 218, Command::CATEGORY_ENERGY, "Save power redirect preset #2" },   //!KEY_SHIFT_F12!218 SHIFT_F12 Save power redirect preset #2
    { 205, Command::CATEGORY_ENERGY, "Use power redirect preset #1" },   //!KEY_F11!205 F11 Use power redirect preset #1
    { 206, Command::CATEGORY_ENERGY, "Use power redirect preset #2" },   //!KEY_F12!206 F12 Use power redirect preset #2
    { 115, Command::CATEGORY_SHIELD, "Cycle shield settings" },   //!KEY_S!115 S Cycle shield settings
    { 8, Command::CATEGORY_PROPULSION, "Full throttle", },   //!KEY_BACKSPACE!8 Backspace Full throttle
    { 93, Command::CATEGORY_PROPULSION, "2 / 3 throttle" },   //!KEY_RIGHT_BRACKET!93] 2 / 3 throttle
    { 91, Command::CATEGORY_PROPULSION, "1 / 3 throttle" },   //!KEY_LEFT_BRACKET!91[1 / 3 throttle
    { 92, Command::CATEGORY_PROPULSION, "Zero throttle" },   //!KEY_FOWARD_SLASH!92 \ Zero throttle
    { 61, Command::CATEGORY_PROPULSION, "Increase throttle" },   //!KEY_EQUAL!61 = (Equal)Increase throttle
    { 45, Command::CATEGORY_PROPULSION, "Decrease throttle" },   //!KEY_MINUS!45 - (Minus)Decrease throttle
    { 13, Command::CATEGORY_PROPULSION, "Match targeted craft's speed" },   //!KEY_ENTER!13 Enter Match targeted craft's speed
    { 97, Command::CATEGORY_TARGET, "Target attacker of target" },    //!KEY_A!97 A Target attacker of target
    { 44, Command::CATEGORY_TARGET, "Cycle through target's components" },   //!KEY_COMMA!44, (Comma)Cycle through target's components
    { 60, Command::CATEGORY_TARGET, "Reverse cycle through target's components" },   //!KEY_LESS_THAN!60 < Reverse cycle through target's components
    { 155, Command::CATEGORY_TARGET, "Pick target in sight" },   //!KEY_ALT_1!155 Alt - 1 Pick target in sight
    { 157, Command::CATEGORY_TARGET, "Roll / Target ship in sights" },   //!KEY_ALT_3!157 Alt - 3 Roll / Target ship in sights
    { 101, Command::CATEGORY_TARGET, "Cycle through fighters targeting you", },   //!KEY_E!101 E Cycle through fighters targeting you
    { 105, Command::CATEGORY_TARGET, "Target nearest incoming warhead" },   //!KEY_I!105 I Target nearest incoming warhead
    { 110, Command::CATEGORY_TARGET, "Target next hyper buoy" },   //!KEY_N!110 N Target next hyper buoy
    { 111, Command::CATEGORY_TARGET, "Target nearest objective craft" },   //!KEY_O!111 O Target nearest objective craft
    { 112, Command::CATEGORY_TARGET, "Target nearest player craft" },   //!KEY_P!112 P Target nearest player craft
    { 114, Command::CATEGORY_TARGET, "Target nearest fighter" },   //!KEY_R!114 R Target nearest fighter
    { 116, Command::CATEGORY_TARGET, "Next target" },   //!KEY_T!116 T Next target
    { 117, Command::CATEGORY_TARGET, "Target newest craft" },   //!KEY_U!117 U Target newest craft
    { 121, Command::CATEGORY_TARGET, "Previous target" },   //!KEY_Y!121 Y Previous target
    { 195, Command::CATEGORY_TARGET, "Target next friendly craft" },   //!KEY_F1!195 F1 Target next friendly craft
    { 196, Command::CATEGORY_TARGET, "Target next neutral craft" },   //!KEY_F2!196 F2 Target next neutral craft
    { 197, Command::CATEGORY_TARGET, "Target next enemy craft" },   //!KEY_F3!197 F3 Target next enemy craft
    { 198, Command::CATEGORY_TARGET, "Target previous enemy craft" },   //!KEY_F4!198 F4 Target previous enemy craft
    { 199, Command::CATEGORY_TARGET, "Select target preset #1 as target" },   //!KEY_F5!199 F5 Select target preset #1 as target
    { 200, Command::CATEGORY_TARGET, "Select target preset #2 as target" },   //!KEY_F6!200 F6 Select target preset #2 as target
    { 201, Command::CATEGORY_TARGET, "Select target preset #3 as target" },   //!KEY_F7!201 F7 Select target preset #3 as target
    { 207, Command::CATEGORY_TARGET, "Target previous friendly craft" },   //!KEY_SHIFT_F1!207 SHIFT - F1 Target previous friendly craft
    { 208, Command::CATEGORY_TARGET, "Target previous neutral craft" },   //!KEY_SHIFT_F2!208 SHIFT - F2 Target previous neutral craft
    { 209, Command::CATEGORY_TARGET, "Target previous enemy craft" },   //!KEY_SHIFT_F3!209 SHIFT - F3 Target previous enemy craft
    { 211, Command::CATEGORY_TARGET, "Save target in target preset #1" },   //!KEY_SHIFT_F5!211 SHIFT - F5 Save target in target preset #1
    { 212, Command::CATEGORY_TARGET, "Save target in target preset #2" },   //!KEY_SHIFT_F6!212 SHIFT - F6 Save target in target preset #2
    { 213, Command::CATEGORY_TARGET, "Save target in target preset #3" },   //!KEY_SHIFT_F7!213 SHIFT - F7 Save target in target preset #3
    { 68, Command::CATEGORY_INTERCRAFT, "Dock with targeted craft" },   //!KEY_SHIFT_D!68 Shift - D Dock with targeted craft
    { 80, Command::CATEGORY_INTERCRAFT, "Pick up targeted craft" },   //!KEY_SHIFT_P!80 Shift - P Pick up targeted craft
    { 82, Command::CATEGORY_INTERCRAFT, "Release carried object" },   //!KEY_SHIFT_R!82 Shift - R Release carried object.
    { 83, Command::CATEGORY_NONE, "Call for reinforcements" },   //!KEY_SHIFT_S!83 Shift - S Call for reinforcements
    { 87, Command::CATEGORY_NONE, "Order craft to stop / wait" },   //!KEY_SHIFT_W!87 Shift - W Order craft to stop / wait
    { 148, Command::CATEGORY_NONE, "Target camera mode" },   //!KEY_ALT_U!148 Alt - U Target camera mode
    { 106, Command::CATEGORY_NONE, "Jump to new craft if available" },   //!KEY_J!106 J Jump to new craft if available
    { 118, Command::CATEGORY_NONE, "Toggle S - Foil" },   //!KEY_V!118 V Toggle S-Foil
    { 49, Command::CATEGORY_NONE, "Wingman command screen option 1" },   //!KEY_1!49 1 Wingman command screen option 1
    { 50, Command::CATEGORY_NONE, "Wingman command screen option 2" },   //!KEY_2!50 2 Wingman command screen option 2
    { 51, Command::CATEGORY_NONE, "Wingman command screen option 3" },   //!KEY_3!51 3 Wingman command screen option 3
    { 52, Command::CATEGORY_NONE, "Wingman command screen option 4" },   //!KEY_4!52 4 Wingman command screen option 4
    { 53, Command::CATEGORY_NONE, "Wingman command screen option 5" },   //!KEY_5!53 5 Wingman command screen option 5
    { 54, Command::CATEGORY_NONE, "Wingman command screen option 6" },   //!KEY_6!54 6 Wingman command screen option 6
    { 55, Command::CATEGORY_NONE, "Wingman command screen option 7" },   //!KEY_7!55 7 Wingman command screen option 7
    { 56, Command::CATEGORY_NONE, "Wingman command screen option 8" },   //!KEY_8!56 8 Wingman command screen option 8
    { 57, Command::CATEGORY_NONE, "Wingman command screen option 9" },   //!KEY_9!57 9 Wingmain command screen option 9
    { 33, Command::CATEGORY_NONE, "Custom Taunt #1" },   //!KEY_SHIFT_1!33 Shift - 1 Custom Taunt #1
    { 64, Command::CATEGORY_NONE, "Custom Taunt #2" },   //!KEY_SHIFT_2!64 Shift - 2 Custom Taunt #2
    { 35, Command::CATEGORY_NONE, "Custom Taunt #3" },   //!KEY_SHIFT_3!35 Shift - 3 Custom Taunt #3
    { 36, Command::CATEGORY_NONE, "Custom Taunt #4" },   //!KEY_SHIFT_4!36 Shift - 4 Custom Taunt #4
    { 65, Command::CATEGORY_NONE, "Assign target to wingmen" },   //!KEY_SHIFT_A!65 Shift - A Assign target to wingmen
    { 66, Command::CATEGORY_NONE, "Signal re - supply ship" },   //!KEY_SHIFT_B!66 Shift - B Signal re - supply ship
    { 67, Command::CATEGORY_NONE, "Order wingmen to cover you" },   //!KEY_SHIFT_C!67 Shift - C Order wingmen to cover you
    { 69, Command::CATEGORY_NONE, "Order targeted craft to evade" },   //!KEY_SHIFT_E!69 Shift - E Order targeted craft to evade
    { 71, Command::CATEGORY_NONE, "Order waiting craft to go" },   //!KEY_SHIFT_G!71 Shift - G Order waiting craft to go
    { 72, Command::CATEGORY_NONE, "Order targeted craft to go home" },   //!KEY_SHIFT_H!72 Shift - H Order targeted craft to go home
    { 73, Command::CATEGORY_NONE, "Order wingmen to ignore target" },   //!KEY_SHIFT_I!73 Shift - I Order wingmen to ignore target
    { 32, Command::CATEGORY_NONE, "Confirm critical orders" },   //!KEY_SPACE!32 Space Confirm critical orders
    { 129, Command::CATEGORY_GAME, "Cycle brightness settings" },   //!KEY_ALT_B!129 Alt - B Cycle brightness settings
    { 130, Command::CATEGORY_GAME, "Clear CMD" },   //!KEY_ALT_C!130 Alt - C Clear CMD
    { 131, Command::CATEGORY_GAME, "Cycle graphics detail settings" },   //!KEY_ALT_D!131 Alt - D Cycle graphics detail settings
    { 146, Command::CATEGORY_GAME, "Toggle system messages on / off" },   //!KEY_ALT_S!146 Alt - S Toggle system messages on / off
    { 149, Command::CATEGORY_GAME, "Displays game version #" },   //!KEY_ALT_V!149 Alt - V Displays game version #
    { 27, Command::CATEGORY_GAME, "Options screen" },   //!KEY_ESCAPE!27 Escape Options screen
    { 132, Command::CATEGORY_GAME, "Eject" },   //!KEY_ALT_E!132 Alt - E Eject
    { 143, Command::CATEGORY_GAME, "Pause game" },   //!KEY_ALT_P!143 Alt - P Pause game
    { 113, Command::CATEGORY_GAME, "Quit mission" },   //!KEY_Q!113 Q Quit mission
    { 9, Command::CATEGORY_GUI, "Bring up wingman command screen in MFD" },   //!KEY_TAB!9 Tab Bring up wingman command screen in MFD
    { 169, Command::CATEGORY_GUI, "Toggle left MFD" },   //!KEY_DELETE!169 Delete Toggle left MFD
    { 173, Command::CATEGORY_GUI, "Toggle right MFD" },   //!KEY_PAGEDOWN!173 PageDown Toggle right MFD
    { 171, Command::CATEGORY_GUI, "Toggle CMD" },   //!KEY_END!171 End Toggle CMD
    { 170, Command::CATEGORY_GUI, "Center indicators" },   //!KEY_HOME!170 Home Center indicators
    { 168, Command::CATEGORY_GUI, "Toggle Left Sensor / Shield Indicator" },   //!KEY_INSERT!168 Insert Toggle Left Sensor / Shield Indicator
    { 172, Command::CATEGORY_GUI, "Toggle Right Sensor / Beam Indicator" },   //!KEY_PAGEUP!172 PageUp Toggle Right Sensor / Beam Indicator
    { 46, Command::CATEGORY_GUI, "Toggle cockpit on / off" },   //!KEY_PERIOD!46 .(Period)Toggle cockpit on / off
    { 137, Command::CATEGORY_VIEW, "Flyby camera mode" },   //!KEY_ALT_J!137 Alt - J Flyby camera mode
    { 141, Command::CATEGORY_VIEW, "Missile camera mode" },   //!KEY_ALT_N!141 Alt - N Missile camera mode
    { 180, Command::CATEGORY_VIEW, "Virtual cockpit look down" },   //!KEY_PAD_2!180 Pad - 2 Virtual cockpit look down
    { 182, Command::CATEGORY_VIEW, "Virtual cockpit look left" },   //!KEY_PAD_4!182 Pad - 4 Virtual cockpit look left
    { 184, Command::CATEGORY_VIEW, "Virtual cockpit look right" },   //!KEY_PAD_6!184 Pad - 6 Virtual cockpit look right
    { 186, Command::CATEGORY_VIEW, "Virtual cockpit look up" },   //!KEY_PAD_8!186 Pad - 8 Virtual cockpit look up
    { 189, Command::CATEGORY_VIEW, "External camera mode" },   //!KEY_PAD_MINUS!189 Pad - / External camera mode
    { 190, Command::CATEGORY_VIEW, "Reposition camera in external mode" },   //!KEY_PAD_STAR!190 Pad - *Reposition camera in external mode
    { 175, Command::CATEGORY_VIEW, "Toggle mouse look mode" },   //!KEY_SCROLL_LOCK!175 Scroll - Lock Toggle mouse look mode
    { 108, Command::CATEGORY_VIEW, "Toggle padlock view on / off" },   //!KEY_L!108 L Toggle padlock view on/off
    { 109, Command::CATEGORY_VIEW, "Map mode" },   //!KEY_M!109 M Map mode
};

ButtonCommandPair CfgButtonMap[XWA_MAX_BUTTONS] =
{
    { &CfgButtonTable[0], &CfgCommandTable[0] },
    { &CfgButtonTable[1], &CfgCommandTable[0] },
    { &CfgButtonTable[2], &CfgCommandTable[0] },
    { &CfgButtonTable[3], &CfgCommandTable[0] },
    { &CfgButtonTable[4], &CfgCommandTable[0] },
    { &CfgButtonTable[5], &CfgCommandTable[0] },
    { &CfgButtonTable[6], &CfgCommandTable[0] },
    { &CfgButtonTable[7], &CfgCommandTable[0] },
    { &CfgButtonTable[8], &CfgCommandTable[0] },
    { &CfgButtonTable[9], &CfgCommandTable[0] },
    { &CfgButtonTable[10], &CfgCommandTable[0] },
    { &CfgButtonTable[11], &CfgCommandTable[0] },
    { &CfgButtonTable[12], &CfgCommandTable[0] },
    { &CfgButtonTable[13], &CfgCommandTable[0] },
    { &CfgButtonTable[14], &CfgCommandTable[0] },
    { &CfgButtonTable[15], &CfgCommandTable[0] },
    { &CfgButtonTable[16], &CfgCommandTable[0] },
    { &CfgButtonTable[17], &CfgCommandTable[0] },
    { &CfgButtonTable[18], &CfgCommandTable[0] },
    { &CfgButtonTable[19], &CfgCommandTable[0] },
};

internal const char* buttonPattern =
    "joybutton1 %d\n"
    "joybutton2 %d\n"
    "joybutton3 %d\n"
    "joybutton4 %d\n"
    "joybutton5 %d\n"
    "joybutton6 %d\n"
    "joybutton7 %d\n"
    "joybutton8 %d\n"
    "joybutton9 %d\n"
    "joybutton10 %d\n"
    "joybutton11 %d\n"
    "joybutton12 %d\n"
    "joybutton13 %d\n"
    "joybutton14 %d\n"
    "joybutton15 %d\n"
    "joybutton16 %d\n"
    "joybutton17 %d\n"
    "joybutton18 %d\n"
    "joybutton19 %d\n"
    "joybutton20 %d\n";

internal Command* GetCommandById(u32 id)
{
    Command* command = NULL;
    for (u32 i = 0; i < XWA_MAX_COMMANDS; ++i)
    {
        command = &CfgCommandTable[i];
        if (command->id == id)
        {
            break;
        }
    }

    return command;
}

XWALauncherStatus CfgLoad(char* path)
{
    XWALauncherStatus status = STATUS_OK;
    char* fileContent = (char*) FileReadInFully(path);
    if (fileContent)
    {
        u32 buttonCommands[XWA_MAX_BUTTONS];
        char* blockBegin = strstr(fileContent, "joybutton1");
        if (blockBegin)
        {
            int result = sscanf(blockBegin, buttonPattern,
                &buttonCommands[0], &buttonCommands[1], &buttonCommands[2],
                &buttonCommands[3], &buttonCommands[4], &buttonCommands[5],
                &buttonCommands[6], &buttonCommands[7], &buttonCommands[8],
                &buttonCommands[9], &buttonCommands[10], &buttonCommands[11],
                &buttonCommands[12], &buttonCommands[13], &buttonCommands[14],
                &buttonCommands[15], &buttonCommands[16], &buttonCommands[17],
                &buttonCommands[18], &buttonCommands[19]
            );
            if (result == 20)
            {
                for (u32 i = 0; i < XWA_MAX_BUTTONS; ++i)
                {
                    Button* button = GetButtonById(i);
                    Command* command = GetCommandById(buttonCommands[i]);
                    command = (command) ? (command) : (&CfgCommandTable[0]);

                    CfgButtonMap[i] = { button, command };
                }
            }
            else
            {
                status = STATUS_ERROR_XWA_CFG_CONTENT;
            }
        }
        else
        {
            status = STATUS_ERROR_XWA_CFG_CONTENT;
        }

        free(fileContent);
    }
    else
    {
        status = STATUS_ERROR_XWA_CFG_READ;
    }

    return status;
}

XWALauncherStatus CfgSave(char* path)
{
    XWALauncherStatus status = STATUS_OK;
    char* fileContent = (char*) FileReadInFully("config.cfg");
    if (fileContent)
    {
        char* blockBegin = strstr(fileContent, "joybutton1");
        char* blockEnd = strstr(fileContent, "joybutton20");
        blockEnd = strstr(blockEnd, "\n");//skip line
        ++blockEnd;//skip \n

        if (blockBegin && blockEnd)
        {
            u32 buttonCommands[XWA_MAX_BUTTONS];
            for (u32 i = 0; i < XWA_MAX_BUTTONS; ++i)
            {
                Button* button = CfgButtonMap[i].button;
                Command* command = CfgButtonMap[i].command;

                buttonCommands[button->id] = command->id;
            }

            char blockContent[4096];
            snprintf(blockContent, sizeof(blockContent), buttonPattern,
                buttonCommands[0], buttonCommands[1], buttonCommands[2],
                buttonCommands[3], buttonCommands[4], buttonCommands[5],
                buttonCommands[6], buttonCommands[7], buttonCommands[8],
                buttonCommands[9], buttonCommands[10], buttonCommands[11], 
                buttonCommands[12], buttonCommands[13], buttonCommands[14],
                buttonCommands[15], buttonCommands[16], buttonCommands[17],
                buttonCommands[18], buttonCommands[19]
            );

            u64 preBlockLength = (blockBegin - fileContent);
            u64 postBlockLength = strlen(blockEnd);
            u64 blockLength = strlen(blockContent);

            u64 newFileLength = (preBlockLength + blockLength + postBlockLength + 1);
            char* newFileContent = (char*) calloc(newFileLength, sizeof(char));
            if (newFileContent)
            {
                char* cursor = newFileContent;
                strncpy(cursor, fileContent, preBlockLength);
                cursor += preBlockLength;
                strncpy(cursor, blockContent, blockLength);
                cursor += blockLength;
                strncpy(cursor, blockEnd, postBlockLength);
                cursor += postBlockLength;

                if (!FileWriteFully(path, newFileContent, newFileLength))
                {
                    status = STATUS_ERROR_XWA_CFG_WRITE;
                }

                free(newFileContent);
            }
            else
            {
                status = STATUS_ERROR_XWA_CFG_CONTENT;
            }
        }
        else
        {
            status = STATUS_ERROR_XWA_CFG_CONTENT;
        }

        free(fileContent);
    }
    else
    {
        status = STATUS_ERROR_XWA_CFG_READ;
    }

    return status;
}