#pragma once

#include "imgui/imgui.h"

//"Callbacks"
extern void ImGuiAppInit();
extern void ImGuiAppFinish();
extern void ImGuiAppUpdate();

//Services
void ImGuiAppTerminate();
void ImGuiAppSetWindowSize(unsigned int width, unsigned int height);
