#ifndef NDEBUG
#pragma comment(linker, "/SUBSYSTEM:CONSOLE")
#else
#define NOCONSOLE
#pragma comment(linker, "/SUBSYSTEM:WINDOWS /ENTRY:mainCRTStartup")
#endif

#include "imgui/build_imgui.cpp"

#include "common.cpp"
#include "cfg_tools.cpp"
#include "exe_tools.cpp"
#include "x_wing_alliance_launcher.cpp"