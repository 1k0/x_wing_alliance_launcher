#include "exe_tools.h"

#include "winnn_dll.h"

internal XWALauncherStatus PatchExe(char* exePath, char* patchedExePath)
{
    //NOTE : always patch the exe, no versioning ATM.
    XWALauncherStatus status = STATUS_OK;

    u64 exeSize = 0;
    char* exeData = (char*) FileReadInFully(exePath, true, &exeSize);
    if (exeData)
    {
        u32 winmmNameOffset = 0x001AC078;
        char* winmmName = exeData + winmmNameOffset;
        if (strnicmp(winmmName, XWA_DLL_NAME, 9) == 0)
        {
            memcpy(winmmName, XWA_MOD_DLL_NAME, 9);

            if (!FileWriteFully(patchedExePath, exeData, exeSize))
            {
                status = STATUS_ERROR_XWA_EXE_WRITE;
            }
        }
        else
        {
            status = STATUS_ERROR_XWA_EXE_CONTENT;
        }

        free(exeData);
    }
    else
    {
        status = STATUS_ERROR_XWA_EXE_READ;
    }

    return status;
}

internal XWALauncherStatus DeployDll(char* dllPath)
{
    //NOTE : always copy the dll, no versioning ATM
    if (!FileWriteFully(dllPath, winmmDllData, sizeof(winmmDllData)))
    {
        return STATUS_ERROR_XWA_DLL_WRITE;
    }

    return STATUS_OK;
}

XWALauncherStatus LaunchGame(char* path)
{
    XWALauncherStatus status = STATUS_OK;
    status = PatchExe(XWA_EXE_NAME, XWA_MOD_EXE_NAME);
    if (status == STATUS_OK)
    {
        status = DeployDll(XWA_MOD_DLL_NAME);
        if (status == STATUS_OK)
        {
            STARTUPINFO info = { sizeof(info) };
            PROCESS_INFORMATION processInfo;
            if (CreateProcess(path, NULL, NULL, NULL, TRUE, 0, NULL, NULL, &info, &processInfo))
            {
                ImGuiAppTerminate();
            }
            else
            {
                status = STATUS_ERROR_XWA_EXE_LAUNCH;
            }
        }
    }

    return status;
}