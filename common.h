#pragma once

#include <stdint.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float f32;
typedef double f64;

typedef size_t msize;

struct Size2U32
{
    u32 width;
    u32 height;
};

struct Color4F32
{
    f32 r;
    f32 g;
    f32 b;
    f32 a;
};

#define ArrayLen(arr) (sizeof(arr) / sizeof(arr[0]))
#define external extern
#define internal static

enum XWALauncherStatus : u32
{
    STATUS_OK = 0,
    STATUS_ERROR_XWA_EXE_READ,
    STATUS_ERROR_XWA_EXE_CONTENT,
    STATUS_ERROR_XWA_EXE_WRITE,
    STATUS_ERROR_XWA_EXE_LAUNCH,
    STATUS_ERROR_XWA_CFG_READ,
    STATUS_ERROR_XWA_CFG_CONTENT,
    STATUS_ERROR_XWA_CFG_WRITE,
    STATUS_ERROR_XWA_DLL_WRITE,
    STATUS_COUNT,
};

#define XWA_CFG_NAME "config.cfg"
#define XWA_EXE_NAME "xwingalliance.exe"
#define XWA_MOD_EXE_NAME "xwingalliance_mod.exe"
#define XWA_DLL_NAME "winmm.dll"
#define XWA_MOD_DLL_NAME "winnn.dll"

bool FileIsPresent(char* path);
void* FileReadInFully(char* path, bool binary = false, msize* size = NULL);
bool FileWriteFully(char* path, void* data, msize size);

void CPrint(const char* format, ...);