# About #

Custom launcher for X-Wing Alliance. Adds XInput support, supports button config up to
20 buttons. Nondestructive, uses a modified copy of xwingalliance.exe, to use a custom
winmm.dll which implements the xinput support.

### Build ###

Use cmake with CMakeLists.txt OR build.cpp with MS Build tools.
Only 64bit Windows build supported at the moment.

### Dependencies ###

OpenGL 2.0 is required for imgui
