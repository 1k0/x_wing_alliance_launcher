#include "common.h"
#include "imgui/imgui_app.h"
#include "cfg_tools.h"
#include "exe_tools.h"


char* XWALauncherStatusToString[] = {
    "ok",
    "could not read " XWA_EXE_NAME,
    "exe content",
    "could not write " XWA_MOD_EXE_NAME,
    "could not launch game",
    "could not read " XWA_CFG_NAME,
    "cfg content",
    "could not write " XWA_CFG_NAME,
    "could not write " XWA_MOD_DLL_NAME,
};

char* XWACommandCategoryToString[] = {
    "none",
    "weapon",
    "energy",
    "shield",
    "propulsion",
    "target",
    "intercraft",
    "game",
    "gui",
    "view"
};

char* GetStatusString(XWALauncherStatus s)
{
    if (s < STATUS_COUNT && s < ArrayLen(XWALauncherStatusToString))
    {
        return XWALauncherStatusToString[s];
    }
    return "unknown";
}

char* GetCommandCategoryString(Command::Category c)
{
    if (c < Command::CATEGORY_COUNT && c < ArrayLen(XWACommandCategoryToString))
    {
        return XWACommandCategoryToString[c];
    }
    return "unknown";
}

struct AppState
{
    bool exeFound;
    bool alertBoxVisible;
    XWALauncherStatus alertBoxError;

    Command::Category commandFilterCategory;
};
AppState appState = {};

void ShowAlertBox(XWALauncherStatus status)
{
    appState.alertBoxError = status;
    appState.alertBoxVisible = true;
    ImGui::OpenPopup("AlertBox");
}

void GuiCenterHorizontally(f32 width)
{
    ImVec2 region = ImGui::GetContentRegionAvail();
    width /= 2.0f;
    region.x /= 2.0f;
    region.x -= width;
    ImGui::SetCursorPosX(region.x);
}

void GuiAlertBox()
{
    if (appState.alertBoxVisible)
    {
        if (ImGui::BeginPopupModal("AlertBox", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove))
        {
            ImGui::Text("ERROR : %s", GetStatusString(appState.alertBoxError));
            
            GuiCenterHorizontally(48);
            if (ImGui::Button("OK", {48, 0}))
            {
                appState.alertBoxVisible = false;
                ImGui::CloseCurrentPopup();
            }
            ImGui::EndPopup();
        }
    }
}

void GuiGamepadButtonPanel()
{
    ImGui::BeginChild("GamepadButtonPanel", { 0, 0 }, true);
    {
        for (u32 i = 0; i < XWA_MAX_BUTTONS; ++i)
        {
            ImGui::PushID(i);
            ImGui::TextUnformatted(CfgButtonMap[i].button->name);

            f32 startFrom = ImGui::GetContentRegionAvailWidth() * 0.35f;
            f32 width = ImGui::GetContentRegionAvailWidth() * 0.65f;

            ImGui::SameLine(startFrom);
            ImGui::PushItemWidth(width);

            if (ImGui::BeginCombo("##hidelabel", CfgButtonMap[i].command->name))
            {
                for (u32 j = 0; j < XWA_MAX_COMMANDS; ++j)
                {
                    if (appState.commandFilterCategory == 0 || 
                        CfgCommandTable[j].category == appState.commandFilterCategory)
                    {
                        ImGui::PushID(j);
                        if (ImGui::Selectable(CfgCommandTable[j].name))
                        {
                            CfgButtonMap[i].command = &CfgCommandTable[j];
                        }
                        ImGui::PopID();
                    }
                }

                ImGui::EndCombo();
            }
            ImGui::PopID();
        }
    }
    ImGui::EndChild();
}

void GuiGamepadHeaderPanel()
{
    ImGui::Separator();

    ImVec2 region = ImGui::GetContentRegionAvail();
    region.x /= 2;
    region.x -= 2; //padding

    if (ImGui::Button("Load", { region.x, 0 }))
    {
        XWALauncherStatus status;
        if ((status = CfgLoad(XWA_CFG_NAME)) != STATUS_OK)
        {
            ShowAlertBox(status);
        }
    }

    ImGui::SameLine(0, 2);

    if (ImGui::Button("Save", { region.x, 0 }))
    {
        XWALauncherStatus status;
        if ((status = CfgSave(XWA_CFG_NAME)) != STATUS_OK)
        {
            ShowAlertBox(status);
        }
    }

    ImGui::TextUnformatted("CommandFilter");

    f32 startFrom = ImGui::GetContentRegionAvailWidth() * 0.3f;
    f32 width = ImGui::GetContentRegionAvailWidth() * 0.7f;

    ImGui::SameLine(startFrom);
    ImGui::PushItemWidth(width);

    if (ImGui::BeginCombo("##hidelabel", GetCommandCategoryString(appState.commandFilterCategory)))
    {
        for (u32 j = 0; j < Command::CATEGORY_COUNT; ++j)
        {
            ImGui::PushID(j);
            if (ImGui::Selectable(GetCommandCategoryString((Command::Category) j)))
            {
                appState.commandFilterCategory = (Command::Category) j;
            }
            ImGui::PopID();
        }

        ImGui::EndCombo();
    }

    ImGui::Separator();
}

void GuiGamepadSettings()
{
    if (ImGui::CollapsingHeader("Gamepad settings"))
    {
        GuiGamepadHeaderPanel();
        GuiGamepadButtonPanel();
    }
}

void GuiReadme()
{
    if (ImGui::CollapsingHeader("Readme"))
    {
        ImGui::TextWrapped("This program is a 3rd party launcher for X-Wing Alliance. Extra features : ");
        ImGui::BulletText("XInput support.");
        ImGui::BulletText("Joystick button config up to 20 buttons.");

        ImGui::TextUnformatted("Usage : ");
        ImGui::BulletText("Copy this program to the installation folder.");
        ImGui::BulletText("(Optional) configure your gamepad button layout.");
        ImGui::BulletText("Launch the game.");
    }
}

void GuiLaunchGame()
{
    ImVec2 region = ImGui::GetContentRegionAvail();
    XWALauncherStatus status = STATUS_OK;

    if (ImGui::ButtonEx("Launch Game", { region.x, 32.0f }, ImGuiButtonFlags_Repeat))
    {
        status = LaunchGame(XWA_MOD_EXE_NAME);
        if (status != STATUS_OK)
        {
            ShowAlertBox(status);
        }
    }
}

void GuiMainMenu()
{
    GuiLaunchGame();
    GuiReadme();
    GuiGamepadSettings();
}

void ImGuiAppInit()
{
    CPrint("ImGuiAppInit\n");
    ImGuiAppSetWindowSize(480, 480);
    ImGui::StyleColorsLight();

    appState.exeFound = FileIsPresent(XWA_EXE_NAME);
}

void ImGuiAppFinish()
{
    CPrint("ImGuiAppFinish\n");
}

void GuiFatalBox(char* message)
{
    ImGui::OpenPopup("Fatal error");

    if (ImGui::BeginPopupModal("Fatal error", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove))
    {
        ImGui::TextWrapped(message);

        GuiCenterHorizontally(48);
        if (ImGui::Button("OK", { 48, 0 }))
        {
            ImGuiAppTerminate();
        }
        ImGui::EndPopup();
    }
}

void ImGuiAppUpdate()
{
    ImGuiIO io = ImGui::GetIO();

    ImVec2 windowPos = { 0.0f, 0.0f };
    ImVec2 windowSize = io.DisplaySize;
    ImGuiWindowFlags windowFlags = (
        ImGuiWindowFlags_NoResize |
        ImGuiWindowFlags_NoMove |
        ImGuiWindowFlags_NoCollapse |
        ImGuiWindowFlags_NoTitleBar
    );
    ImGui::SetNextWindowPos(windowPos); 
    ImGui::SetNextWindowSize(windowSize);

    if (appState.exeFound == false)
    {
        GuiFatalBox("Error : xwingalliance.exe not found. Please launch the program from the installation directory!");
    }
    else
    {
        ImGui::Begin("LauncherWindow", 0, windowFlags);
        {
            GuiAlertBox();
            GuiMainMenu();
        }
        ImGui::End();
    }
}