#pragma once

#include "common.h"

#define BUTTON_D_UP 0
#define BUTTON_D_DOWN 1
#define BUTTON_D_LEFT 2
#define BUTTON_D_RIGHT 3
#define BUTTON_START 4
#define BUTTON_BACK 5
#define BUTTON_L3 6
#define BUTTON_R3 7
#define BUTTON_L1 8
#define BUTTON_R1 9
#define BUTTON_L2 10
#define BUTTON_R2 11
#define BUTTON_A 12
#define BUTTON_B 13
#define BUTTON_X 14
#define BUTTON_Y 15
#define BUTTON_RA_UP 16
#define BUTTON_RA_RIGHT 17
#define BUTTON_RA_DOWN 18
#define BUTTON_RA_LEFT 19

#define XWA_MAX_BUTTONS 20
#define XWA_MAX_COMMANDS 113

struct Button
{
    u32 id;
    char* name;
};

struct Command
{
    enum Category : u32
    {
        CATEGORY_NONE = 0,
        CATEGORY_WEAPON,
        CATEGORY_ENERGY,
        CATEGORY_SHIELD,
        CATEGORY_PROPULSION,
        CATEGORY_TARGET,
        CATEGORY_INTERCRAFT,
        CATEGORY_GAME,
        CATEGORY_GUI,
        CATEGORY_VIEW,
        CATEGORY_COUNT,
    };

    u32 id;
    Category category;
    char* name;
};

struct ButtonCommandPair
{
    Button* button;
    Command* command;
};

external Button CfgButtonTable[XWA_MAX_BUTTONS];
external Command CfgCommandTable[XWA_MAX_COMMANDS];
external ButtonCommandPair CfgButtonMap[XWA_MAX_BUTTONS];

XWALauncherStatus CfgLoad(char* path);
XWALauncherStatus CfgSave(char* path);