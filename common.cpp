#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

bool FileIsPresent(char* path)
{
    DWORD attribs = GetFileAttributesA(path);
    return (attribs != INVALID_FILE_ATTRIBUTES);
}

void* FileReadInFully(char* path, bool binary, msize* size)
{
    void* contents = NULL;
    FILE* file = NULL;

    if (FileIsPresent(path))
    {
        char* mode = (binary) ? ("rb") : ("r");
        FILE* file = fopen(path, mode);
        if (file)
        {
            fseek(file, 0, SEEK_END);
            msize length = ftell(file);
            if (size)
            {
                (*size) = length;
            }

            fseek(file, 0, SEEK_SET);
            contents = (void*) calloc(length, sizeof(char));
            if (contents)
            {
                fread(contents, sizeof(char), length, file);
            }

            fclose(file);
        }
    }

    return contents;
}

bool FileWriteFully(char* path, void* data, msize size)
{
    DWORD written = 0;
    HANDLE file = CreateFileA(
        path,
        GENERIC_WRITE,
        0,
        NULL,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );
    if (file != INVALID_HANDLE_VALUE)
    {
        WriteFile(file, data, size, &written, NULL);
        CloseHandle(file);
    }

    return (written == size);
}

void CPrint(const char* format, ...)
{
#ifndef NOCONSOLE
    va_list args;
    va_start(args, format);
    printf(format, args);
    va_end(args);
#endif
}